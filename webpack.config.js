var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        app: './src/main.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'main.bundle.js'
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    module:{
        rules:[
            
            {
                test: /\.(png|jp(e*)g|svg)$/,  
                use: [{
                    loader: 'url-loader',
                    options: { 
                        limit: 80000, // Convert images < 8mb to base64 strings
                        name: 'images/[hash]-[name].[ext]'
                    } 
                }]
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(scss|css|sass)$/,
                use: [{
                loader: "style-loader" // creates style nodes from JS strings
                }, {
                loader: "css-loader" // translates CSS into CommonJS
                }, {
                loader: "sass-loader" // compiles Sass to CSS
                }]
            },
            {
                test: /\.(png|jp(e*)g|svg)$/,
                use: {
                  loader: "file-loader",
                  options: {
                    name: 'images/[hash]-[name].[ext]'
                  },
                },
            }
       ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            //'process.env.NODE_ENV': JSON.stringify('production')
            'process.env.NODE_ENV': JSON.stringify('dev')
    }) ],
    devServer: {
        contentBase: path.join(__dirname, "/"),
        compress: true,
        inline: true,
        port: 8888,
        open: true,
        publicPath: "/dist/",
        hot: true,
        watchOptions: {
                poll: true
            }
        },
    stats: { colors: true }
};